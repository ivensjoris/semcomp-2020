import React from 'react';
import {
	Create,
	Datagrid,
	DateField,
	Edit,
	List,
	Show,
	SimpleForm,
	SimpleShowLayout,
	TextField,
	TextInput,
	ReferenceField,
	ShowButton,
	EditButton,
	DeleteButton,
	ReferenceInput,
	ReferenceArrayInput,
	SelectInput,
	AutocompleteInput,
	SelectArrayInput,
	ReferenceArrayField,
	SingleFieldList,
	ChipField,
	ImageField,
	Filter,
} from 'react-admin';
import { DateTimeInput } from 'react-admin-date-inputs';
import DateFnsUtils from '@date-io/date-fns';
import locale from "date-fns/locale/pt-BR";

const ScheduleFilter = props => (
	<Filter {...props}>
		<TextInput label="Buscar por Titulo" source="title" alwaysOn />
		<ReferenceInput label="Palestrante" source="speaker_id" reference="speaker" perPage={100} alwaysOn>
			<SelectInput optionText="name" />
		</ReferenceInput>
	</Filter>
);

export const ScheduleList = props => (
	<List {...props} filters={<ScheduleFilter />}>
		<Datagrid>
			<TextField source="title" />
			<TextField source="description"
				style={{
					whiteSpace: 'nowrap',
					overflow: 'hidden',
					textOverflow: 'ellipsis',
					maxWidth: 250,
					display: 'block',
				}}
			/>
			<ReferenceField label="Palestrante" source="speaker_id" reference="speaker">
				<TextField source="name" />
			</ReferenceField>
			<ReferenceField label="Localização" source="places_id" reference="places">
				<TextField source="place" />
			</ReferenceField>
			<ReferenceField label="Tipo de Programa" source="types_id" reference="types">
				<TextField source="name" />
			</ReferenceField>
			<ReferenceArrayField label="Tags" reference="tags" source="tags_ids">
				<SingleFieldList>
					<ChipField source="name" />
				</SingleFieldList>
			</ReferenceArrayField>
			<DateField source="end_time" />
			<DateField source="start_time" />
			<ShowButton label="Visualizar" />
			<EditButton label="Editar" />
			<DeleteButton label="Deletar" redirect={false} />
		</Datagrid>
	</List>
);

export const ScheduleEdit = props => (
	<Edit {...props}>
		<SimpleForm>
			<TextField source="id" />
			<TextInput source="title" label="Titulo" />
			<TextInput multiline source="description" label="Descrição" />
			<DateTimeInput source="start_time" label="Inicia em" options={{ format: 'dd/MM/yyyy, HH:mm:ss', ampm: false, clearable: true }} providerOptions={{ utils: DateFnsUtils, locale: locale }} />
			<DateTimeInput source="end_time" label="Acaba em" options={{ format: 'dd/MM/yyyy, HH:mm:ss', ampm: false, clearable: true }} providerOptions={{ utils: DateFnsUtils, locale: locale }} />
			<ReferenceInput
				label="Palestrante"
				source="speaker_id"
				reference="speaker"
				perPage={100}
				sort={{ field: 'name', order: 'ASC' }}
			>
				<SelectInput label="Palestrante" optionText="name" />
			</ReferenceInput>
			<ReferenceInput
				label="Lugar"
				source="places_id"
				reference="places"
			>
				<SelectInput label="Lugar" optionText="place" />
			</ReferenceInput>
			<ReferenceInput
				label="Tipo de Programa"
				source="types_id"
				reference="types"
			>
				<SelectInput label="Tipo de Programa" optionText="name" />
			</ReferenceInput>
			<ReferenceArrayInput
				label="Tags"
				source="tags_ids"
				reference="tags"
			>
				<SelectArrayInput label="Tags" optionText="name" />
			</ReferenceArrayInput>
			<DateField source="createdate" label="Criado em" />
			<TextField source="createdby" label="Criado por" />
			<DateField source="lastupdate" label="Ultima vez atualizado" />
			<TextField source="updatedby" label="Atualizado por" />
		</SimpleForm>
	</Edit>
);

export const ScheduleCreate = props => (
	<Create {...props}>
		<SimpleForm>
			<TextInput source="title" label="Titulo" />
			<TextInput multiline source="description" label="Descrição" />
			<DateTimeInput source="start_time" label="Inicia em" options={{ format: 'dd/MM/yyyy, HH:mm:ss', ampm: false, clearable: true }} providerOptions={{ utils: DateFnsUtils, locale: locale }} />
			<DateTimeInput source="end_time" label="Acaba em" options={{ format: 'dd/MM/yyyy, HH:mm:ss', ampm: false, clearable: true }} providerOptions={{ utils: DateFnsUtils, locale: locale }} />
			<ReferenceInput
				label="Palestrante"
				source="speaker_id"
				reference="speaker"
				perPage={100}
				sort={{ field: 'name', order: 'ASC' }}
			>
				<SelectInput label="Palestrante" optionText="name" />
			</ReferenceInput>
			<ReferenceInput
				label="Lugar"
				source="places_id"
				reference="places"
			>
				<SelectInput label="Lugar" optionText="place" />
			</ReferenceInput>
			<ReferenceInput
				label="Tipo de Programa"
				source="types_id"
				reference="types"
			>
				<SelectInput label="Tipo de Programa" optionText="name" />
			</ReferenceInput>
			<ReferenceArrayInput
				label="Tags"
				source="tags_ids"
				reference="tags"
			>
				<SelectArrayInput label="Tags" optionText="name" />
			</ReferenceArrayInput>
		</SimpleForm>
	</Create>
);

export const ScheduleShow = props => (
	<Show {...props}>
		<SimpleShowLayout>
			<TextField source="id" />
			<TextField source="title" label="Titulo" />
			<TextField multiline source="description" label="Descrição" />
			<DateField source="start_time" label="Inicia em" />
			<DateField source="end_time" label="Acaba em" />
			<ReferenceField
				label="Palestrante"
				source="speaker_id"
				reference="speaker"
			>
				<TextField source="name" />

			</ReferenceField>
			<ReferenceField
				label="Foto do Palestrante"
				source="speaker_id"
				reference="speaker"
			>
				<ImageField
					source="picture.src"
					title="picture.title"
				/>
			</ReferenceField>
			<ReferenceField
				label="Lugar"
				source="places_id"
				reference="places"
			>
				<TextField source="place" />
			</ReferenceField>
			<ReferenceField
				label="Tipo de Programa"
				source="types_id"
				reference="types"
			>
				<TextField source="name" />
			</ReferenceField>
			<ReferenceArrayField
				label="Tags"
				source="tags_ids"
				reference="tags"
			>
				<SingleFieldList>
					<ChipField source="name" />
				</SingleFieldList>
			</ReferenceArrayField>
			<DateField source="createdate" label="Criado em" />
			<TextField source="createdby" label="Criado por" />
			<DateField source="lastupdate" label="Ultima vez atualizado" />
			<TextField source="updatedby" label="Atualizado por" />
		</SimpleShowLayout>
	</Show>
);

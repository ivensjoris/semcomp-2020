import React from 'react'
import {
	List,
	Edit,
	Create,
	Show,
	SimpleForm,
	SimpleShowLayout,
	TextInput,
	ImageInput,
	ImageField,
	DateField,
	Datagrid,
	TextField,
	RichTextField,
	Filter,
	ShowButton,
	EditButton,
	DeleteButton,
} from 'react-admin'

const SpeakerFilter = props => (
	<Filter {...props}>
		<TextInput label="Buscar por nome" source="name" alwaysOn />
	</Filter>
);

export const SpeakerList = props => (
	<List {...props} filters={<SpeakerFilter />}>
		<Datagrid>
			<TextField source="name" label="Nome" />
			<TextField source="description" label="Descrição"
				style={{
					whiteSpace: 'nowrap',
					overflow: 'hidden',
					textOverflow: 'ellipsis',
					maxWidth: 250,
					display: 'block',
				}}
			/>
			<ImageField
				label="Foto"
				source="picture.src"
				title="picture.title"
			/>
			<ShowButton label="Visualizar" />
			<EditButton label="Editar" />
			<DeleteButton label="Deletar" redirect={false} />
		</Datagrid>
	</List>
);

export const SpeakerEdit = props => (
	<Edit {...props}>
		<SimpleForm>
			<TextField source="id" />
			<TextInput source="name" label="Nome" />
			<TextInput multiline source="description" label="Descrição" />
			<ImageInput source="picture" label="Foto" accept="image/*">
				<ImageField source="src" title="title" />
			</ImageInput>
			<DateField source="createdate" label="Criado em" />
			<TextField source="createdby" label="Criado por" />
			<DateField source="lastupdate" label="Ultima vez atualizado" />
			<TextField source="updatedby" label="Atualizado por" />
		</SimpleForm>
	</Edit>
);

export const SpeakerCreate = props => (
	<Create {...props}>
		<SimpleForm>
			<TextInput source="name" label="Nome" />
			<TextInput multiline source="description" label="Descrição" />
			<ImageInput source="picture" label="Foto" accept="image/*">
				<ImageField source="src" title="title" />
			</ImageInput>
		</SimpleForm>
	</Create>
);

export const SpeakerShow = props => (
	<Show {...props}>
		<SimpleShowLayout>
			<TextField source="id" />
			<TextField source="name" label="Nome" />
			<RichTextField source="description" label="Descrição" />
			<ImageField
				label="Foto"
				source="picture.src"
				title="picture.title"
			/>
			<DateField source="createdate" label="Criado em" />
			<TextField source="createdby" label="Criado por" />
			<DateField source="lastupdate" label="Ultima vez atualizado" />
			<TextField source="updatedby" label="Atualizado por" />
		</SimpleShowLayout>
	</Show>
);

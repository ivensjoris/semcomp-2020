import React from 'react';
import {
	Create,
	Datagrid,
	DateField,
	Edit,
	List,
	Show,
	SimpleForm,
	SimpleShowLayout,
	TextField,
	TextInput,
} from 'react-admin';

export const TypeList = props => (
	<List {...props}>
		<Datagrid rowClick="edit">
			<TextField source="name" label="Nome" />
		</Datagrid>
	</List>
);

export const TypeEdit = props => (
	<Edit {...props}>
		<SimpleForm>
			<TextField source="id" />
			<TextInput source="name" label="Nome" />
			<DateField source="createdate" label="Criado em" />
			<TextField source="createdby" label="Criado por" />
			<DateField source="lastupdate" label="Ultima vez atualizado" />
			<TextField source="updatedby" label="Atualizado por" />
		</SimpleForm>
	</Edit>
);

export const TypeCreate = props => (
	<Create {...props}>
		<SimpleForm>
			<TextInput source="name" label="Nome" />
		</SimpleForm>
	</Create>
);

export const TypeShow = props => (
	<Show {...props}>
		<SimpleShowLayout>
			<TextField source="id" />
			<TextField source="name" label="Nome" />
			<DateField source="createdate" label="Criado em" />
			<TextField source="createdby" label="Criado por" />
			<DateField source="lastupdate" label="Ultima vez atualizado" />
			<TextField source="updatedby" label="Atualizado por" />
		</SimpleShowLayout>
	</Show>
);

import React from 'react';
import {
	Create,
	Datagrid,
	DateField,
	Edit,
	List,
	Show,
	SimpleForm,
	SimpleShowLayout,
	TextField,
	TextInput,
} from 'react-admin';

export const TagsList = props => (
	<List {...props}>
		<Datagrid rowClick="edit">
			<TextField source="name" label="Nome" />
		</Datagrid>
	</List>
);

export const TagsEdit = props => (
	<Edit {...props}>
		<SimpleForm>
			<TextField source="id" />
			<TextInput source="name" label="Nome" />
			<DateField source="createdate" label="Criado em" />
			<TextField source="createdby" label="Criado por" />
			<DateField source="lastupdate" label="Ultima vez atualizado" />
			<TextField source="updatedby" label="Atualizado por" />
		</SimpleForm>
	</Edit>
);

export const TagsCreate = props => (
	<Create {...props}>
		<SimpleForm>
			<TextInput source="name" label="Nome" />
		</SimpleForm>
	</Create>
);

export const TagsShow = props => (
	<Show {...props}>
		<SimpleShowLayout>
			<TextField source="id" />
			<TextField source="name" label="Nome" />
			<DateField source="createdate" label="Criado em" />
			<TextField source="createdby" label="Criado por" />
			<DateField source="lastupdate" label="Ultima vez atualizado" />
			<TextField source="updatedby" label="Atualizado por" />
		</SimpleShowLayout>
	</Show>
);

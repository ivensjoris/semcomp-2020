import React from 'react'
import {
	List,
	Edit,
	Create,
	Show,
	SimpleForm,
	SimpleShowLayout,
	TextInput,
	ImageInput,
	ImageField,
	DateField,
	Datagrid,
	TextField,
	RichTextField,
	Filter,
	ShowButton,
	EditButton,
	DeleteButton,
	UrlField,
	ChipField,
	CheckboxGroupInput,
} from 'react-admin'

const SponsorsFilter = props => (
	<Filter {...props}>
		<TextInput label="Buscar por nome" source="name" alwaysOn />
	</Filter>
);

export const SponsorsList = props => (
	<List {...props} filters={<SponsorsFilter />}>
		<Datagrid>
			<TextField source="name" label="Nome" />
			<UrlField source="site" label="Site" />
			<ImageField
				label="Foto"
				source="picture.src"
				title="picture.title"
			/>
			<ChipField source="category" />
			<ShowButton label="Visualizar" />
			<EditButton label="Editar" />
			<DeleteButton label="Deletar" redirect={false} />
		</Datagrid>
	</List>
);

export const SponsorsEdit = props => (
	<Edit {...props}>
		<SimpleForm>
			<TextField source="id" />
			<TextInput source="name" label="Nome" />
			<TextInput source="site" label="Site" />
			<ImageInput source="picture" label="Foto" accept="image/*">
				<ImageField source="src" title="title" />
			</ImageInput>
			<CheckboxGroupInput source="category" choices={[
				{ id: '1', name: 'tipo 1' },
				{ id: '2', name: 'tipo 2' },
				{ id: '3', name: 'tipo 3' },
			]} />
			<DateField source="createdate" label="Criado em" />
			<TextField source="createdby" label="Criado por" />
			<DateField source="lastupdate" label="Ultima vez atualizado" />
			<TextField source="updatedby" label="Atualizado por" />
		</SimpleForm>
	</Edit>
);

export const SponsorsCreate = props => (
	<Create {...props}>
		<SimpleForm>
			<TextInput source="name" label="Nome" />
			<TextInput source="site" label="Site" />
			<ImageInput source="picture" label="Foto" accept="image/*">
				<ImageField source="src" title="title" />
			</ImageInput>
			<CheckboxGroupInput source="category" label="Categoria" choices={[
				{ id: '1', name: 'tipo 1' },
				{ id: '2', name: 'tipo 2' },
				{ id: '3', name: 'tipo 3' },
			]} />
		</SimpleForm>
	</Create>
);

export const SponsorsShow = props => (
	<Show {...props}>
		<SimpleShowLayout>
			<TextField source="id" />
			<TextField source="name" label="Nome" />
			<UrlField source="site" label="Site" />
			<ImageField
				label="Foto"
				source="picture.src"
				title="picture.title"
			/>
			<ChipField source="category" />
			<DateField source="createdate" label="Criado em" />
			<TextField source="createdby" label="Criado por" />
			<DateField source="lastupdate" label="Ultima vez atualizado" />
			<TextField source="updatedby" label="Atualizado por" />
		</SimpleShowLayout>
	</Show>
);

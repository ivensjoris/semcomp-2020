import React from 'react'
import {
	List,
	Edit,
	Create,
	Show,
	SimpleForm,
	SimpleShowLayout,
	DateField,
	TextInput,
	Datagrid,
	TextField,
	ShowButton,
	EditButton,
	DeleteButton,
} from 'react-admin'

export const PlaceList = props => (
	<List {...props}>
		<Datagrid>
			<TextField source="place" label="Lugar" />
			<TextField source="createdby" label="criado por" />
			<DateField source="lastupdate" label="ultima vez atualizado" />
			<TextField source="id" />
			<ShowButton label="Visualizar" />
			<EditButton label="Editar" />
			<DeleteButton label="Deletar" redirect={false} />
		</Datagrid>
	</List>
);

export const PlaceEdit = props => (
	<Edit {...props}>
		<SimpleForm>
			<TextInput source="place" label="lugar" />
			<TextField source="id" />
			<DateField source="createdate" label="Criado em" />
			<TextField source="createdby" label="Criado por" />
			<DateField source="lastupdate" label="Ultima vez atualizado" />
			<TextField source="updatedby" label="Atualizado por" />
		</SimpleForm>
	</Edit>
);

export const PlaceCreate = props => (
	<Create {...props}>
		<SimpleForm>
			<TextInput source="place" label="lugar" />
		</SimpleForm>
	</Create>
);

export const PlaceShow = props => (
	<Show {...props}>
		<SimpleShowLayout>
			<TextField source="id" />
			<TextField source="place" />
			<DateField source="createdate" label="Criado em" />
			<TextField source="createdby" label="Criado por" />
			<DateField source="lastupdate" label="Ultima vez atualizado" />
			<TextField source="updatedby" label="Atualizado por" />
		</SimpleShowLayout>
	</Show>
);

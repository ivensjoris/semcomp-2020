import * as React from "react";
import { Admin, Resource } from "react-admin";
import {
  FirebaseDataProvider,
  FirebaseAuthProvider
} from "react-admin-firebase";
import firebase from 'firebase';
import UserIcon from '@material-ui/icons/People';
import CommentIcon from '@material-ui/icons/Comment';
import PlaceIcon from '@material-ui/icons/Place'
import LabelIcon from '@material-ui/icons/Label'
import CreditCardIcon from '@material-ui/icons/CreditCard'
import AssignmentIcon from '@material-ui/icons/Assignment'

import { firebaseConfig } from './FIREBASE_CONFIG';
import CustomLoginPage from './CustomLoginPage';
import { PlaceList, PlaceEdit, PlaceCreate, PlaceShow } from "./pages/places";
import { SpeakerList, SpeakerEdit, SpeakerCreate, SpeakerShow } from "./pages/speaker";
import { TypeList, TypeEdit, TypeShow, TypeCreate } from "./pages/type";
import { ScheduleList, ScheduleEdit, ScheduleCreate, ScheduleShow } from "./pages/schedule";
import { TagsList, TagsShow, TagsEdit, TagsCreate } from "./pages/tags";
import { SponsorsList, SponsorsShow, SponsorsEdit, SponsorsCreate } from "./pages/sponsors";

const firebaseApp = firebase.initializeApp(firebaseConfig);

const options = {
  logging: true,
  app: firebaseApp,
  // rootRef: 'rootrefcollection/QQG2McwjR2Bohi9OwQzP',
  // watch: ['posts'];
  // dontwatch: ['comments'];
}

const authProvider = FirebaseAuthProvider(firebaseConfig, options);
const dataProvider = FirebaseDataProvider(firebaseConfig, options);

class App extends React.Component {
  render() {
    return (
      <Admin
        loginPage={CustomLoginPage}
        dataProvider={dataProvider}
        authProvider={authProvider}
      >
        <Resource
          name="speaker"
          options={{ label: 'Palestrantes' }}
          icon={UserIcon}
          list={SpeakerList}
          show={SpeakerShow}
          edit={SpeakerEdit}
          create={SpeakerCreate}
        />
        <Resource
          name="schedule"
          options={{ label: 'Programas' }}
          icon={CommentIcon}
          list={ScheduleList}
          show={ScheduleShow}
          edit={ScheduleEdit}
          create={ScheduleCreate}
        />
        <Resource
          name="sponsors"
          options={{ label: 'Patrocinadores' }}
          icon={CreditCardIcon}
          list={SponsorsList}
          show={SponsorsShow}
          edit={SponsorsEdit}
          create={SponsorsCreate}
        />
        <Resource
          name="places"
          icon={PlaceIcon}
          options={{ label: 'Locais' }}
          list={PlaceList}
          show={PlaceShow}
          edit={PlaceEdit}
          create={PlaceCreate}
        />
        <Resource
          name="types"
          options={{ label: 'Tipos de Programa' }}
          icon={AssignmentIcon}
          list={TypeList}
          show={TypeShow}
          edit={TypeEdit}
          create={TypeCreate}
        />
        <Resource
          name="tags"
          options={{ label: 'Tags' }}
          icon={LabelIcon}
          list={TagsList}
          show={TagsShow}
          edit={TagsEdit}
          create={TagsCreate}
        />
      </Admin>
    );
  }
}

export default App;

import React, { useState, useRef, useEffect } from "react";
import Container, {
  Main,
  VideoOverlay,
  PageContainer,
  Menu,
  ul,
  ContentContainer,
  Summary,
  Card,
  FooterSection,
  DivingMan
} from "./styles";
import divingImg from "../assets/diving-man.png";
import logoImg from "../assets/logo.png";
import { Helmet } from "react-helmet";

import {
  Instagram as InstaIcon,
  MessageCircle as WhatsappIcon,
  Mail as EmailIcon,
  ChevronRight as ArrowRightIcon,
  ChevronDown as ArrowDownIcon
} from "react-feather";
import shuffle from "shuffle-array";
import {
  Modal,
  Backdrop,
  Fade,
  makeStyles,
  List,
  ListSubheader,
  ListItem,
  ListItemIcon,
  ListItemText,
  Collapse
} from "@material-ui/core";

import "./reset.css";
import bgVidePreview from "../assets/bgvideo-preview.png";
import { useStaticQuery, graphql as gql } from "gatsby";
import { format } from "date-fns";

const useStyles = makeStyles(theme => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    color: "#fff",
    outline: "none",
    padding: 32,
    maxWidth: 800,
    margin: "0 auto"
  },
  paper: {
    backgroundColor: "#00001D",
    border: "2px solid #000010",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    fontSize: "1.1rem",
    color: "#fff",
    outline: "none"
  },
  root: {
    width: "100%",
    background: "#111",
    color: "#fff",
    marginTop: 38
    // maxWidth: 360
  },
  nested: {
    background: "#111",
    paddingLeft: theme.spacing(4)
  },
  listItem: {
    color: "#FAFAFA !important"
  }
}));

export default () => {
  const classes = useStyles();
  const [yPos, setYpos] = useState(0);
  const [currentSpeaker, setCurrentSpeaker] = useState();
  const [currentShedule, setCurrentShedule] = useState();
  const [isChrome, setIsChrome] = useState();
  let scrollListener = () => {
    const winScroll =
      document.body.scrollTop || document.documentElement.scrollTop;

    const pos = winScroll; // height;

    setYpos(pos);
  };
  useEffect(() => {
    if (typeof window !== "undefined") {
      window.addEventListener("scroll", scrollListener);
      setIsChrome(window.chrome);
    }
  }, []);

  const data = useStaticQuery(gql`
    {
      allSpeaker {
        nodes {
          name
          id
          description
          picture {
            src
          }
        }
      }
      allSchedules(sort: { fields: start_time }) {
        nodes {
          id
          speaker_id
          title
          end_time
          start_time
          description
        }
      }
    }
  `);

  const [speakers, setSpeakers] = useState();
  const [allSpeakers, setAllSpeakers] = useState([]);
  const [showAll, setShowAll] = useState(false);
  useEffect(() => {
    if (data) {
      console.log({ data });
      const withPictures = data.allSpeaker.nodes.filter(
        speaker => speaker.picture
      );
      setAllSpeakers(withPictures);
      const withNoPictures = data.allSpeaker.nodes.filter(speaker =>
        withPictures.map(s => s.id).includes(speaker.id)
      );
      let aditionals = [];
      for (let i = withPictures.length; i < 5; ) {
        aditionals.push(
          withNoPictures[Math.floor(withNoPictures.length * Math.random())]
        );
      }
      const total = [...withPictures, ...aditionals];
      setSpeakers(shuffle.pick(total, { picks: 6 }));
      data.allSchedules.nodes = data.allSchedules.nodes.map(node => ({
        ...node,
        speaker: data.allSpeaker.nodes.find(
          speakerNode => speakerNode.id === node.speaker_id
        )
      }));
    }
  }, [data]);
  const scrollToRef = ref => window.scrollTo(0, ref.current.offsetTop - 120);
  const refContato = useRef(null);
  const refProposta = useRef(null);
  const executeScroll = ref => scrollToRef(ref);
  const dates = {
    "18": new Date("2020-03-18T00:00:00.000Z"),
    "19": new Date("2020-03-19T00:00:00.000Z"),
    "20": new Date("2020-03-20T00:00:00.000Z"),
    "21": new Date("2020-03-21T00:00:00.000Z")
  };
  const [selectedDate, setSelectedDate] = useState(dates["19"]);

  return (
    <>
      <Helmet defer={false}>
        <title>Semcomp 2020 - Semana de Computação na UFBA 2020</title>
        <meta
          name="description"
          content="A SEMCOMP UFBA é um evento que visa fomentar o empreendedorismo e movimentar a comunidade de computação."
        />
        <meta
          name="keywords"
          content="Evento, Computação, Tecnologia, Empreendedorismo, Inovação, Salvador, Bahia, Sistemas, Inteligência Artificial, Universidade Federal, Nordeste, Engenharia, Realidade Aumentada, Computação Quântica, Bitcoin, Blockchain"
        />
        <meta name="robots" content="" />
        <meta name="revisit-after" content="7 day" />
        <meta name="language" content="Portuguese" />
        <meta name="generator" content="N/A" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <html lang="pt-br" />
      </Helmet>
      <Container>
        <img
          src={bgVidePreview}
          alt="Imagem de fundo de oceano"
          className="video"
        />
        <VideoOverlay />
        <PageContainer>
          <svg>
            <filter id="turbulence" x="0" y="0" width="100%" height="100%">
              <feTurbulence
                height="100%"
                id="sea-filter"
                numOctaves="1"
                seed="3"
                baseFrequency="5"
              ></feTurbulence>
              <feDisplacementMap
                scale="8"
                in="SourceGraphic"
              ></feDisplacementMap>
              <animate
                xlinkHref="#sea-filter"
                attributeName="baseFrequency"
                dur="60s"
                keyTimes="0;0.5;1"
                values="0.02 0.06;0.04 0.08;0.02 0.06"
                repeatCount="indefinite"
              />
            </filter>
          </svg>
          <DivingMan isChrome={Boolean(isChrome)} alt="" src={divingImg} />

          <ContentContainer>
            <Menu className="menu" yPos={yPos}>
              <menu>
                <ul>
                  <li>
                    <a href="#">INÍCIO</a>
                  </li>

                  <li>
                    <a onClick={() => executeScroll(refProposta)}>PROPOSTA</a>
                  </li>
                  <li>
                    <a onClick={() => executeScroll(refContato)}>CONTATO</a>
                  </li>
                </ul>
                <ul>
                  <li>
                    <a
                      target="_blank"
                      href="https://docs.google.com/forms/d/e/1FAIpQLScUYBaTHnMuw8DXWGyrMRq4INhhdv45oWjGUAxzTI3tCvj5Uw/viewform"
                    >
                      PATROCINAR
                    </a>
                  </li>
                  <li>
                    <a
                      target="_blank"
                      href="https://credencial.imasters.com.br/semcomp-ufba-2020"
                    >
                      INSCREVA-SE
                    </a>
                  </li>
                </ul>
              </menu>
            </Menu>
            <Main>
              <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={!!currentSpeaker}
                onClose={() => setCurrentSpeaker()}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                  timeout: 500
                }}
              >
                <Fade in={!!currentSpeaker}>
                  <div className={classes.paper}>
                    {currentSpeaker && (
                      <img
                        src={currentSpeaker.picture.src}
                        css={
                          "width: 140px; height: 140px; background: white; margin-top: 15px; object-fit: cover;"
                        }
                      />
                    )}
                    <h2 id="transition-modal-title" css={"margin: 8px 0"}>
                      {currentSpeaker && currentSpeaker.name}
                    </h2>
                    <p id="transition-modal-description">
                      {currentSpeaker && currentSpeaker.description}
                    </p>
                  </div>
                </Fade>
              </Modal>
              <Summary className="summary">
                <img
                  src={logoImg}
                  alt="logomarca da semcomp 2020"
                  className="logo"
                />
                <p>18 a 21 de março de 2020</p>
              </Summary>
              <section id="proposta" ref={refProposta} className="about">
                <p className="intro">
                  A SEMCOMP UFBA (Semana de Computação da UFBA) foi criada para
                  incentivar e inspirar o empreendedorismo baiano, movimentando
                  toda a comunidade de tecnologia de nosso estado.
                </p>
                <p>
                  Em 2020, na oitava edição do evento, entre os dias 18 e 21 de
                  março, a SEMCOMP conta com uma programação completa e
                  diversificada. Venha se inspirar, criar e compartilhar com a
                  gente.{" "}
                  
                </p>
                <a
                    target="_blank"
                    href="https://credencial.imasters.com.br/semcomp-ufba-2020"
                    css={`background: white; padding: 8px 12px; border-radius: 4px; color: #000; padding-right: 32px; display: inline;`}
                  >
                    Garanta já seu ingresso
                              {" "} <ArrowRightIcon
                                css={`
                                margin-top: 2px;
                                  min-width: 24px !important;
                                `}
                              />
                  </a>
              </section>
              <section className="section">
                <h1>ALGUNS DOS CONFIRMADOS ATÉ O MOMENTO</h1>
                <h2>Vai ficar fora dessa?</h2>
                <div className="speaker-container" css={`position: relative; padding-bottom: 38px;`}>
                  {speakers &&
                    speakers.map(speaker => (
                      <div key={speaker.id} className="speaker">
                        {speaker.picture ? (
                          <img
                            src={speaker.picture.src}
                            onClick={() => setCurrentSpeaker(speaker)}
                          />
                        ) : (
                          <span
                            className="no-photo"
                            onClick={() => setCurrentSpeaker(speaker)}
                          />
                        )}
                        <span>{speaker.name}</span>
                      </div>
                    ))}
                    {!showAll && <button css={`
                          background: transparent;
                          border: none;
                          padding: 4px;
                          color: #fafafa;
                          text-transform: uppercase;
                          right: 0;
                          position: absolute;
                          bottom : 0;
                          font-size: 1rem;
                          font-weight: bold;
                          :hover{
                            cursor: pointer;
                          }
                          `} 
                          onClick={() => setShowAll(true)} >Ver Todos >> </button>}
                    {showAll && allSpeakers.filter(({ id }) => !speakers.map(s => s.id).includes(id)).map(speaker => (
                      <div key={speaker.id} className="speaker">
                      {speaker.picture ? (
                        <img
                          src={speaker.picture.src}
                          onClick={() => setCurrentSpeaker(speaker)}
                        />
                      ) : (
                        <span
                          className="no-photo"
                          onClick={() => setCurrentSpeaker(speaker)}
                        />
                      )}
                      <span>{speaker.name}</span>
                    </div>
                    ))}
                </div>
                <div>
                  <List
                    component="nav"
                    aria-labelledby="nested-list-subheader"
                    subheader={
                      <ListSubheader
                        css={"color: #fff !important"}
                        component="div"
                        id="nested-list-subheader"
                      >
                        Grade do Evento (em construção)
                      </ListSubheader>
                    }
                    className={classes.root}
                  >
                    {Object.keys(dates).map(dayNumber => (
                      <>
                        <ListItem
                          button
                          onClick={() => setSelectedDate(dates[dayNumber])}
                        >
                          <ListItemIcon
                            css={`
                              font-size: 0.8rem;
                              color: #fafafa99 !important;
                              padding-right: 16px;
                              min-width: 24px !important;
                            `}
                          >
                            {selectedDate.toISOString() !==
                            dates[dayNumber].toISOString() ? (
                              <ArrowRightIcon
                                css={`
                                  min-width: 24px !important;
                                  margin-top: -12px;
                                `}
                              />
                            ) : (
                              <ArrowDownIcon
                                css={`
                                  min-width: 24px !important;
                                  margin-top: -12px;
                                `}
                              />
                            )}
                          </ListItemIcon>
                          <ListItemText
                            primary={`${dayNumber}/03/2020`}
                            css={`
                              flex-grow: 0 !important;
                              margin-right: 18px;
                            `}
                          />
                          {dayNumber === "18" ? (
                            <i css="background: #fff; color: #000; border-radius: 4px; font-size: 0.6rem; font-weight: bold; padding: 6px; box-sizing: border-box;">
                              Em breve!
                            </i>
                          ) : (
                            ""
                          )}
                          {/* {open ? <ExpandLess /> : <ExpandMore />} */}
                          {/* {dates[dayNumber].toISOString()} */}
                        </ListItem>
                        <Collapse
                          in={
                            selectedDate.toISOString() ===
                            dates[dayNumber].toISOString()
                          }
                          timeout="auto"
                          unmountOnExit
                        >
                          <List component="div" disablePadding>
                            {data.allSchedules?.nodes
                              .filter(
                                node =>
                                  new Date(node.start_time).getDate() ==
                                    dayNumber && node.title !== "---"
                              )
                              .map(node => (
                                <>
                                <ListItem button onClick={() => node.id === currentShedule ? setCurrentShedule(undefined) : setCurrentShedule(node.id)} className={classes.nested}>
                                  <ListItemIcon
                                    css={`
                                      font-size: 0.8rem;
                                      color: #fafafa99 !important;
                                      padding-right: 16px;
                                    `}
                                  >
                                    {format(new Date(node.start_time), "HH:mm")}
                                    <br />
                                    {format(new Date(node.end_time), "HH:mm")}
                                  </ListItemIcon>
                                  <ListItemText
                                    primary={node.title}
                                    secondary={
                                      node.speaker && node.speaker.name
                                    }
                                    className={classes.listItem}
                                  />
                                </ListItem>
                                <Collapse
                                  in={
                                    currentShedule === node.id
                                  }
                                  timeout="auto"
                                  unmountOnExit
                                >
                                  <div css={`padding: 0 96px; font-size: 1rem;`}>
                                    {node.description}
                                </div>
                              </Collapse>
                                </>
                              ))}
                          </List>
                        </Collapse>
                      </>
                    ))}
                  </List>
                </div>
              </section>
              <section className="section">
                <h1>APOIE A SEMCOMP</h1>
                <h2>Vamos transformar o mercado juntos?</h2>
                <p>
                  <span>
                    Para ajudar a SEMCOMP a inspirar cada vez mais a comunidade
                    baiana de tecnologia, que tal dar uma olhada na{" "}
                    <a target="_blank" href="http://bit.ly/PropostaSEMCOMP2020">
                      Proposta do Evento
                    </a>{" "}
                    e em nosso{" "}
                    <a
                      target="_blank"
                      href="http://bit.ly/patrocinioSEMCOMP2020"
                    >
                      Edital de Patrocínio
                    </a>
                    ? Entre em contato pelo{" "}
                    <a
                      target="_blank"
                      href="https://forms.gle/P9gD1qKjRAxGBWhj6"
                    >
                      nosso formulário
                    </a>{" "}
                    e iremos entrar em contato. :)
                  </span>
                </p>
                <p>
                  São diversas formas de apoiar, além de sempre estarmos abertos
                  a novas propostas e parcerias. Ficou interessado(a)?! Sua
                  empresa, projeto ou ideia podem fazer parte da SEMCOMP 2020.
                </p>
              </section>
              {/*<section className="section">
                <h1>FAÇA PARTE DA EQUIPE</h1>
          
                <p>
                  Você poderá ajudar-nos a produzir o evento! Se você é
                  organizado, proativo e cheio de ideias pra ajudar, entra na
                  nossa onda! Basta se inscrever{" "}
                  <a target="_blank" href="https://bit.do/semcomp2020">
                    neste form
                  </a>{" "}
                  para fazer parte da <b>SeaCrew.</b>
                </p>
                                  </section>*/}
              {/* https://api.whatsapp.com/send?phone=5571992244613&text=Ol%C3%A1!%20Vi%20o%20contato%20no%20site%20do%20gabinete%20do%20texto */}
              <section className="section">
                <h1>OCEANO</h1>
                <h2>Ideias que fluem, constroem e salvam</h2>
                <p>
                  Em 2019, o descaso com o meio ambiente e o crime ambiental
                  foram pauta em todo o litoral do Nordeste. Com isso,
                  identificamos a necessidade de reagir e contribuir para dar
                  visibilidade e soluções para este problema. A organização do
                  evento, junto a coordenadoria de Marketing elegeram OCEANO
                  como o tema matriz da SEMCOMP 2020, conectando o evento ao
                  assunto e convidando os participantes e toda a comunidade a
                  refletirem sobre nossas águas.
                </p>
              </section>
              <FooterSection
              // style={{
              //   display: "grid",
              //   gridColumn: "1/3",
              //   gridTemplateColumns: "1fr 1fr",
              //   gridGap: 30
              // }}
              >
                <section
                  ref={refContato}
                  className="section card-container"
                  id="contato"
                >
                  <h1>Contato</h1>
                  <Card
                    href="https://www.instagram.com/semcompufba/"
                    target="_blank"
                  >
                    <div className="icon">
                      <InstaIcon color="#fafafa" size={30} />
                    </div>
                    <div className="content">
                      <b>Instagram</b>
                      <a>@semcompufba</a>
                    </div>
                  </Card>
                  <Card
                    href="https://api.whatsapp.com/send?phone=557188178613&text=Ol%C3%A1!%20Vi%20o%20contato%20no%20site%20da%20semcomp%202020"
                    // target="_blank"
                    target="_blank"
                  >
                    <div className="icon">
                      <WhatsappIcon color="#fafafa" size={30} />
                    </div>
                    <div className="content">
                      <b>Whatsapp</b>
                      <a>+55 (71) 8743-6097</a>
                    </div>
                  </Card>
                  <Card href="mailto:semcomp@infojr.com.br" target="_blank">
                    <div className="icon">
                      <EmailIcon color="#fafafa" size={30} />
                    </div>
                    <div className="content">
                      <b>Email</b>
                      <a>semcomp@infojr.com.br</a>
                    </div>
                  </Card>
                </section>
                <iframe
                  width="100%"
                  height="396"
                  // style={{
                  //   marginTop: 180,
                  //   boxSizing: "border-box"
                  //   // paddingTop: 40
                  // }}
                  // frameborder="0"
                  // style="border:0"
                  src="https://www.google.com/maps/embed/v1/place?key=AIzaSyBYNRc-AcUv6CRaATV2_AJ8zcYpvOp7uQU
    &q=Pavilhao+de+Aulas+Reitor+Felipe+Serpas+UFBA+Salvador,Ba+Brasil"
                  allowFullScreen
                ></iframe>
              </FooterSection>
            </Main>
          </ContentContainer>
        </PageContainer>
      </Container>
    </>
  );
};

import styled from "styled-components";
import bgVidePreview from "../assets/bgvideo-preview.png";

export default styled.div`
  /* // background: linear-gradient(
  //     186.29deg,
  //     rgba(1, 15, 24, 0.42) 4.99%,
  //     #000000 96.72vh
  //   ),
  //   #010b0f; */

  background: linear-gradient(to bottom, #010108, #000);
  color: #fafafa;
  margin: 0;
  padding: 0;
  width: 100vw;
  overflow: hidden;
  min-height: 100vh;
  position: relative;
  margin: 0 auto;

  .video {
    /* height: auto; */
    width: 100%;
    position: absolute;
    left: 0;
    top: 0;
    z-index: 0;
    min-width: 100vw;
    height: 56.25vw;

    @media screen and (max-width: 664px) {
      min-height: 374px;
      padding: 0;
    }
  }
`;

export const PageContainer = styled.div`
  color: #fafafa;
  /* display: grid;
  grid-template-columns: 1fr minmax(450px, 1fr) minmax(450px, 1fr) 1fr; */
  margin: 0;
  padding: 0;
  width: 100%;
  max-width: 1500px;
  position: relative;
  overflow-x: visible;
  margin: 0 auto;
  /* min-height: 100vh; */
  min-height: 1740px;
  /* filter: url("#bubblue-turbulence"); */

  svg {
    position: absolute;
  }
`;

export const DivingMan = styled.img`
  position: absolute;
  width: 1300px;
  top: -200px;
  z-index: 200;
  right: -340px;
  transition: all 1s ease-in-out;
  @media screen and (min-width: 687px) {
    -webkit-filter: url("#turbulence");
    filter: url("#turbulence");
  }

  @media screen and (min-width: 687px) {
    /* webkit-filter: url("#turbulence") !important; */

    filter: ${(props) => props.isChrome ? "none" : 'url("#turbulence")'} !important;
  }

  @media screen and (max-width: 750px) {
    opacity: 0.6;
  }

  @media screen and (max-width: 700px) {
    opacity: 0.4;
  }

  @media screen and (max-width: 670px) {
    top: -120px;
    opacity: 0.3;
  }
`;


export const ContentContainer = styled.div`
  .MuiTypography-colorTextSecondary {
    color: rgba(255, 255, 255, 0.6);
  }
  position: relative;
  width: 100%;
  height: 100%;
  min-height: 1740px;
  display: grid;
  grid-template-columns: 1fr 940px 1fr;
  @media screen and (max-width: 940px) {
    grid-template-columns: 0 1fr 0;
  }
  align-content: flex-start;
  /* grid-template-rows: 60px auto-f; */
  overflow: hidden;
  z-index: 300;
`;

export const Main = styled.main`
  grid-column: 2/3;
  display: grid;
  grid-template-columns: 1fr 1fr;
  color: #fafafa;
  box-sizing: border-box;
  font-size: 1.24rem;
  padding: 0 20px;
  @media screen and (max-width: 950px) {
    padding: 0 34px;
  }
  .about {
    margin-top: 180px;
    @media screen and (max-width: 670px) {
      grid-column: 1/3;
    }
  }

  .section {
    margin-top: 80px;
    grid-column: 1/3;

    .speaker-container {
      margin-top: 18px;
      display: grid;
      grid-template-columns: repeat(auto-fit, minmax(120px, 1fr));
      grid-gap: 16px 32px;
      align-items: flex-start;

      .speaker {
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        text-align: center;
        img,
        .no-photo {
          object-fit: cover;
          width: 80px;
          height: 80px;
          border-radius: 80px;
          filter: sepia(1) hue-rotate(180deg);
          border: 3px solid #333;
          :hover {
            cursor: pointer;
          }
        }
        .no-photo {
          background: #666;
        }
      }
    }

    h1 {
      margin-bottom: 0;
    }

    h2 {
      font-size: 1.2rem;
      margin: 8px 0;
    }

    p {
      margin-top: 0;
    }

    p a {
      text-decoration: underline;
    }
  }

  .card-container {
    display: grid;
    grid-template-columns: 1fr;
    grid-column: 1/2;
    grid-gap: 10px;
    /* h1 {
      grid-column: 1/2;
    } */
    /* padding-bottom: 180px; */

    /* @media screen and (max-width: 1024px) {
      grid-template-columns: repeat(2, 1fr);

      h1 {
        grid-column: 1/2;
      }
    }

    @media screen and (max-width: 687px) {
      grid-template-columns: 1fr;

      h1 {
        grid-column: 1/2;
      }
    } */
  }
`;

export const Card = styled.a`
  width: 100%;
  min-height: 120px;
  box-sizing: border-box;
  padding: 0 12px;
  border-radius: 4px;
  box-shadow: 1px 8px 30px #000;
  background: #10101d;
  transition: transform 0.3s cubic-bezier(0.68, -0.55, 0.27, 1.55);
  transform: translateY(0px);
  :hover {
    transform: translateY(-18px);
    cursor: pointer;
    text-decoration: none;
  }

  display: grid;
  grid-template-columns: 80px 1fr;
  align-content: center;
  justify-content: center;
  justify-items: center;
  /* align-items: center; */

  .icon {
    box-sizing: border-box;
    padding: 20px;
    width: 100%;
    color: #fafafa;
  }
  .content {
    display: flex;
    flex-direction: column;
    justify-content: center;
    justify-self: flex-start;
  }

  a {
    margin: 4px 0;
    /* font-size: 6rem; */
  }
`;

export const VideoOverlay = styled.div`
  background: linear-gradient(
    183.6deg,
    rgba(0, 0, 0, 0) -65.61%,
    #010102 86.22%
  );
  width: 100vw;
  min-width: 664px;
  /* min-height: 334px; */
  padding-bottom: 56.25%;
  z-index: 100;
  height: 0;
  position: absolute;

  @media screen and (max-width: 664px) {
    min-height: 374px;
    padding: 0;
  }
`;

export const Menu = styled.nav`
  display: grid;
  align-items: center;
  grid-column: 2/3;
  height: 80px;
  width: 100%;
  left: 0;
  position: fixed;
  z-index: 50;
  transition: background 0.3s ease-in-out;
  background: ${p => (p.yPos > 60 ? "#10101d" : "transparent")};
  grid-template-columns: 1fr;
  @media screen and (min-width: 1500px) {
    grid-template-columns: 1fr 1500px 1fr;
    menu {
      grid-column: 2/2;
    }
  }

  menu {
    max-width: 1500px;
    padding: 0 12px 0;
    margin: 0;
    flex-grow: 1;
    display: flex;
    box-sizing: border-box;
    @media screen and (max-width: 950px) {
      flex-direction: column;
      ul {
        width: 100%;
      }
    }
  }
  ul {
    list-style: none;
    display: flex;
    justify-content: space-evenly;
    padding: 0;
    margin: 0;
    width: 50%;
  }
  li {
    margin: 0;
    padding: 0;
    a {
      cursor: pointer;
      font-size: 1.17rem;
    }
    :first-child {
      a {
        text-decoration: none;
      }
    }
  }
  a {
    color: #ddd;
    text-decoration: none;

    /* text-decoration: line-through; */

    :hover {
      text-decoration: underline;
      /* text-decoration: line-through; */
    }
  }
`;

export const Summary = styled.div`
  grid-column: 1/3;
  margin-top: 180px;
  box-sizing: border-box;
  /* padding: 0 20px; */
  width: 50%;
  display: flex;
  flex-direction: column;
  transition: width 1s ease-in-out;

  align-items: flex-start;
  @media screen and (max-width: 670px) {
    width: 100%;
    align-items: center;
  }
  @media screen and (max-width: 950px) {
    /* align-items: center; */
  }

  img.logo {
    width: 305px;
    min-width: 307px;
    @media screen and (max-width: 670px) {
      width: 70%;
      margin-left: 0;
    }
  }
  p {
    text-align: center;
    margin-top: 4px;
    font-size: 1.6rem;
  }
`;

export const FooterSection = styled.div`
  display: grid;
  grid-column: 1/3;
  grid-template-columns: 1fr 1fr;
  grid-gap: 30px;
  padding-bottom: 80px;

  iframe {
    margin-top: 160px;
    box-sizing: border-box;
  }
  @media screen and (max-width: 670px) {
    grid-template-columns: 1fr;
    .card-container {
    }
    iframe {
      margin-top: 0;
    }
  }
`;

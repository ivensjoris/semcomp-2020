/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.org/docs/gatsby-config/
 */

module.exports = {
  siteMetadata: {
    title: `Semcomp 2020`,
    siteUrl: `https://www.semcomp.inforjr.com.br`,
    description: `Semana de Computação na UFBA, de 18 a 21 de abril de 2020`,
    social: [
      {
        name: "instagram",
        url: "https://www.instagram.com/semcompufba/"
      }
    ]
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-sitemap`,
    {
      resolve: `gatsby-plugin-styled-components`,
      options: {
        displayName: process.env.NODE_ENV !== "production"
      }
    },
    {
      resolve: "gatsby-plugin-robots-txt",
      options: {
        host: "https://www.semcomp.com.br",
        sitemap: "https://www.semcomp.com.br/sitemap.xml",
        policy: [{ userAgent: "*", allow: "/" }]
      }
    },
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-plugin-prefetch-google-fonts`,
      options: {
        fonts: [
          {
            family: `Nunito Sans`,
            subsets: [`latin`],
            variants: [`400`, `700`]
          }
        ]
      }
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Semcomp 2020`,
        short_name: `Semcomp`,
        start_url: `/`,
        background_color: `#010B0F`,
        theme_color: `#004673`,
        display: `standalone`,
        icon: "src/assets/favicon.png" //512x512
      }
    },
    {
      resolve: "gatsby-source-firestore",
      options: {
        credential: require("./firebase.json"),
        types: [
          {
            type: "Place",
            collection: "places",
            map: doc => doc
          },
          {
            type: "Schedules",
            collection: "schedule",
            map: doc => doc
          },
          {
            type: "Speaker",
            collection: "speaker",
            map: doc => doc
          },
          {
            type: "Sponsor",
            collection: "sponsors",
            map: doc => doc
          },
          {
            type: "Tag",
            collection: "tags",
            map: doc => doc
          },
          {
            type: "Type",
            collection: "types",
            map: doc => doc
          }
        ]
      }
    },
    {
      resolve: `gatsby-plugin-page-creator`,
      options: {
        path: `${__dirname}/src/pages`,
        ignore: [`**/styles.js`]
      }
    },
    `gatsby-plugin-offline`
  ]
};
